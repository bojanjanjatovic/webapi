﻿using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;

namespace ApiTestClient
{
    class Program
    {
        static void Main(string[] args)
        {
            
            var client = new RestClient("https://kisoft80.eu.auth0.com/oauth/token");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            //authorization with client credentials
            request.AddParameter("application/json", "{\"client_id\":\"3HrVkuGuViPkjVcIIZCdVCHb98isSNcq\",\"client_secret\":\"qAkIod2ypTiYpOBYxxWNL1uhEP-Dh__rbHN9IZ25Hz0luZ7Nufv4HBVAJVw9LUSP\",\"audience\":\"https://customers/api\",\"grant_type\":\"client_credentials\"}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);

            var token = GetToken(response.Content);


            //creating new customer
            request = new RestRequest(Method.POST);
            request.AddHeader("authorization", "Bearer " + token);
            request.AddHeader("content-type", "application/json");           
           
            request.AddJsonBody(new WebApi.Models.Customer { Id = 1, CustomerName = "Test1" });

            client = new RestClient("http://localhost:3010/api/create-customer");
 
            response = client.Execute(request);


            //creating new customer
            request = new RestRequest(Method.POST);
            request.AddHeader("authorization", "Bearer " + token);
            request.AddHeader("content-type", "application/json");

            request.AddJsonBody(new WebApi.Models.Customer { Id = 2, CustomerName = "Test2" });

            response = client.Execute(request);

            //creating new customer, demonstrating unauthorized request
            request = new RestRequest(Method.POST);
            request.AddHeader("authorization", "Bearer ");
            request.AddHeader("content-type", "application/json");

            request.AddJsonBody(new WebApi.Models.Customer { Id = 3, CustomerName = "Test3" });

            response = client.Execute(request);

            client = new RestClient("http://localhost:3010/api/read-customers");


            //reading all customers via non secure endpoint
            request = new RestRequest(Method.GET); 

            response = client.Execute(request);
           
            //deleting one customer from in memory storage
            client = new RestClient("http://localhost:3010/api/delete-customer");

            request = new RestRequest(Method.POST);
            request.AddHeader("authorization", "Bearer " + token);
            request.AddHeader("content-type", "application/json");

            request.AddJsonBody(new WebApi.Models.Customer { Id = 2, CustomerName = "Test2" });

            response = client.Execute(request);


            //confirming that customer id deleted
            client = new RestClient("http://localhost:3010/api/read-customers");

            request = new RestRequest(Method.GET);

            response = client.Execute(request);

        }

        private static string GetToken(string input)
        {
            JObject json = JObject.Parse(input);
            return json.SelectToken("access_token").ToString();
        }
    }
}
