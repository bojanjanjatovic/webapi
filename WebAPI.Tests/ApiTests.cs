﻿using System;
using System.Collections.Generic;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Newtonsoft.Json.Linq;
using RestSharp;

namespace WebAPI.Tests
{
    [TestClass]
    public class ApiTests
    {
        [TestMethod]
        public void TestGetToken()
        {
            var client = new RestClient("https://kisoft80.eu.auth0.com/oauth/token");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", "{\"client_id\":\"3HrVkuGuViPkjVcIIZCdVCHb98isSNcq\",\"client_secret\":\"qAkIod2ypTiYpOBYxxWNL1uhEP-Dh__rbHN9IZ25Hz0luZ7Nufv4HBVAJVw9LUSP\",\"audience\":\"https://customers/api\",\"grant_type\":\"client_credentials\"}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);            

            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.OK);
        }

        [TestMethod]
        public void TestGetTokenNotSuccessefully()
        {
            var client = new RestClient("https://kisoft80.eu.auth0.com/oauth/token");
            var request = new RestRequest(Method.POST);
            request.AddHeader("content-type", "application/json");
            request.AddParameter("application/json", "{\"client_id\":\"3HrVkuGuViPcIIZCdVCHb98isSNcq\",\"client_secret\":\"qAkIod2ypTiYpOBYxxWNL1uhEP-Dh__rbHN9IZ25Hz0luZ7Nufv4HBVAJVw9LUSP\",\"audience\":\"https://customers/api\",\"grant_type\":\"client_credentials\"}", ParameterType.RequestBody);
            IRestResponse response = client.Execute(request);
            Assert.IsTrue(response.StatusCode == System.Net.HttpStatusCode.Unauthorized);
        }

        [TestMethod]
        public void TestCreateCustomer()
        {
            WebApi.Controllers.CustomersController controller = new WebApi.Controllers.CustomersController();

            var response = controller.CreateCustomer(new WebApi.Models.Customer { Id = 1, CustomerName = "Test1" });
            
            Assert.IsTrue(response.GetType() == typeof(System.Web.Http.Results.OkResult));
        }

        [TestMethod]
        public void TestDuplicateCustomerId()
        {
            WebApi.Controllers.CustomersController controller = new WebApi.Controllers.CustomersController();

            var response = controller.CreateCustomer(new WebApi.Models.Customer { Id = 1, CustomerName = "Test1" });

            response = controller.CreateCustomer(new WebApi.Models.Customer { Id = 1, CustomerName = "Test2" });

            Assert.IsTrue(response.GetType() == typeof(System.Web.Http.Results.BadRequestErrorMessageResult));
        }

        [TestMethod]
        public void TestReadCustomers()
        {
            WebApi.Controllers.CustomersController controller = new WebApi.Controllers.CustomersController();

            var response = controller.CreateCustomer(new WebApi.Models.Customer { Id = 1, CustomerName = "Test1" });
           

            response = controller.CreateCustomer(new WebApi.Models.Customer { Id = 2, CustomerName = "Test2" });
            

            response = controller.ReadCustomers();
            
            Assert.IsTrue(response.GetType() == typeof(System.Web.Http.Results.OkNegotiatedContentResult<List<WebApi.Models.Customer>>));
        }

        [TestMethod]
        public void TestDeleteCustomer()
        {
            WebApi.Controllers.CustomersController controller = new WebApi.Controllers.CustomersController();

            controller.CreateCustomer(new WebApi.Models.Customer { Id = 1, CustomerName = "Test1" });
          
            controller.CreateCustomer(new WebApi.Models.Customer { Id = 2, CustomerName = "Test2" });

            var responseExisting = controller.DeleteCustomer(new WebApi.Models.Customer { Id = 1, CustomerName = "Test1" });
            var responseNonExisting = controller.DeleteCustomer(new WebApi.Models.Customer { Id = 3, CustomerName = "Test3" });

            Assert.IsTrue(responseExisting.GetType() == typeof(System.Web.Http.Results.OkResult));
            Assert.IsTrue(responseNonExisting.GetType() == typeof(System.Web.Http.Results.NotFoundResult));
        }



    }
}
