﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Repositories.Entities;
using WebApi.Repositories.Interface;

namespace WebApi.Repositories
{
    public class InMemoryRepository<T> : ICommonRepository<T> where T : IEntity
    {
        public static List<T> _allItems = new List<T>();
        public List<T> AllItems
        {
            get
            {
                return _allItems;
            }
        }

        public IQueryable<T> AsQuery()
        {
            return AllItems.AsQueryable();
        }

        public  bool Add(T item)
        {

            if (AllItems.Any(i => i.Id == item.Id))
                return false;

            AllItems.Add(item);  
            return true;
        }

        public bool Remove(T item)
        {
            if (!_allItems.Any(c => c.Id == item.Id)) 
                return false;

            AllItems.RemoveAll(c => c.Id == item.Id);
            return true;
        }
    }
}