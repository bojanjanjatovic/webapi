﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Repositories.Interface;

namespace WebApi.Repositories.Entities
{
    public class Customer : IEntity
    {
        public int Id { get; set ; }
        public string CustomerName { get; set; }
    }
}