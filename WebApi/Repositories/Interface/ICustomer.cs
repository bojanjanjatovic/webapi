﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApi.Repositories.Entities;

namespace WebApi.Repositories.Interface
{
    public interface ICustomer : ICommonRepository<Customer>
    {

        new IQueryable<Customer> AsQuery();
        new bool Add(Customer item);
        new bool Remove(Customer item);
    }
}