﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Repositories.Interface
{
    public interface ICommonRepository<T> where T : IEntity
    {
        IQueryable<T> AsQuery();
        bool Add(T item);
        bool Remove(T item);
    }
}