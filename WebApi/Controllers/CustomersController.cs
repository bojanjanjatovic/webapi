﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Web.Http;
using System.Web.Http.Description;
using WebApi.Repositories;

namespace WebApi.Controllers
{
    [RoutePrefix("api")]
    public class CustomersController : System.Web.Http.ApiController
    {

        private CustomersRepository _customers = new CustomersRepository();

        [HttpGet]
        [Route("read-customers")]
        [ResponseType(typeof(List<Models.Customer>))]
        public IHttpActionResult ReadCustomers()
        {

            List<Models.Customer> resultList = new List<Models.Customer>();

            for (int i = 0; i < _customers.AllItems.Count; i++)
            {
                resultList.Add(new Models.Customer { Id = _customers.AllItems[i].Id, CustomerName = _customers.AllItems[i].CustomerName });
            }

            return Ok(resultList);

        }

        [HttpPost]
        [Route("delete-customer")]
        [Authorize]
        public IHttpActionResult DeleteCustomer(Models.Customer item)
        {

            if (item == null || !ModelState.IsValid)
            {
                return BadRequest();
            }


            WebApi.Repositories.Entities.Customer customerToRemove = new Repositories.Entities.Customer { Id = item.Id, CustomerName = item.CustomerName };

            var result = _customers.Remove(customerToRemove);

            if (result)
            {
                return Ok();
            }

            return NotFound();
            
        }


        [Route("create-customer")]
        [Authorize]
        public IHttpActionResult CreateCustomer(Models.Customer item)
        {
            if (item == null || !ModelState.IsValid)
            {
                return BadRequest();
            }

            Repositories.Entities.Customer newCustomer = new Repositories.Entities.Customer { Id = item.Id, CustomerName = item.CustomerName };

            var result =  _customers.Add(newCustomer);
            if (result)
            {
                return Ok();
            }

            return BadRequest(string.Format("Customer with Id = {0} already exists", item.Id));

            
        }

        [Authorize]
        [Route("claims")]
        [HttpGet]
        public object Claims()
        {
            var claimsIdentity = User.Identity as ClaimsIdentity;

            return claimsIdentity.Claims.Select(c =>
            new
            {
                Type = c.Type,
                Value = c.Value
            });
        }
    }
}